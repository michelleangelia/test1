<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email_Notification_Model extends MY_Model
{
	public $table = 'email_notifications';
	public $primary_key = 'id';

	public function __construct()
	{
		$this->timestamps = array('created');
		$this->timestamps_format = 'timestamp';
		$this->soft_deletes = true;

		parent::__construct();
	}
}
