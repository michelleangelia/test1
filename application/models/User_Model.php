<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends MY_Model
{
	public $table = 'users';
	public $primary_key = 'id';

	public function __construct()
	{
		$this->timestamps = array('created', 'updated', 'deleted', 'confirmed');
		$this->timestamps_format = 'timestamp';
		$this->soft_deletes = true;

		parent::__construct();
	}
}
