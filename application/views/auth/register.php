<div class="container-fluid page-body-wrapper full-page-wrapper">
    <div class="content-wrapper d-flex align-items-center auth register-full-bg">
        <div class="row w-100">
            <div class="col-lg-4 mx-auto">
                <div class="auth-form-light text-left p-5">
                    <h2>Register</h2>
                    <h4 class="font-weight-light">Hello! let's get started</h4>
                    <form method="POST" action="<?php echo base_url('register/save')?>" id="form-register">
                        <div id="message" style="color:red"></div>
                        <div class="form-group">
                            <label for="first-name">First Name</label>
                            <input type="text" class="form-control" id="first-name" placeholder="First Name" name="first_name" required autofocus>
                            <i class="mdi mdi-account"></i>
                        </div>
                        <div class="form-group">
                            <label for="last-name">Last Name</label>
                            <input type="text" class="form-control" id="last-name" placeholder="Last Name" name="last_name" required autofocus>
                            <i class="mdi mdi-account"></i>
                        </div>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" placeholder="Username" name="username" required>
                            <i class="mdi mdi-account"></i>
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" id="email" placeholder="Email" name="email" required>
                            <i class="mdi mdi-account"></i>
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" id="phone" placeholder="Phone" name="phone" required>
                            <i class="mdi mdi-account"></i>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
                            <i class="mdi mdi-eye"></i>
                        </div>
                        <div class="form-group">
                            <label for="password-confirm">Confirm Password</label>
                            <input type="password" class="form-control" id="password-confirm" placeholder="Confirm password"  name="password_confirmation" required>
                            <i class="mdi mdi-eye"></i>
                        </div>
                        <div class="mt-5">
                            <button type="submit" class="btn btn-primary">Register</button>
                        </div>          
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- content-wrapper ends -->
</div>
