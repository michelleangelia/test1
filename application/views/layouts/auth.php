<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Test</title>
    <link rel="stylesheet" href="assets/dist/css/styles.css">
    <link rel="shortcut icon" href="assets/images/favicon.png" />
</head>

<body>
    <div class="container-scroller">
        <?php echo $body; ?>
    </div>

    <script type="text/javascript">
        var baseUrl = '<?php echo base_url(); ?>';
    </script>
    <script type="text/javascript" src="assets/dist/js/app.js"></script>
    <script type="text/javascript" src="assets/src/js/register.js"></script>
</body>
</html>