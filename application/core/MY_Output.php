<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * MY Output
 * @author Romi D. Sitorus <romi@softwareseni.com>
 * @copyright 2016 Romi D. Sitorus <romi@softwareseni.com>
 */
class MY_Output extends CI_Output {
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Handle HTTP response with correct status code
	 * @param mixed $data
	 * @param int $code
	 * @param string $message
	 * @param bool $rest
	 * @return JSON
	 */
	public function http_response($data = null, $code = 200, $message = 'OK', $rest = true)
	{
		$response = array(
			'status' => array(
				'code' => $code,
				'message' => $message,
			)
		);

		if (!empty($data)) {
			$response['data'] = $data;

			if (!$rest)
				$response = $data;
		}

		$this->set_status_header($code)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode(
				$response,
				JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
			))
			->_display();

		exit();
	}

	/**
	 * Handle HTTP response for Datatable result
	 * @param array $data
	 * @param int $total_rows
	 * @param int $total_filtered
	 * @return JSON
	 */
	public function datatable_response($data, $total_rows = 0, $total_filtered = 0)
	{
		$response = array(
			'data' => $data,
			'recordsTotal' => $total_rows,
			'recordsFiltered' => $total_filtered
		);

		$this->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode(
				$response,
				JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
			))
			->_display();

		exit();
	}
}
