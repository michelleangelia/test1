<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrate extends CI_Controller {

	/**
	 * 
	 */
	public function __construct()
	{
		parent::__construct();
		$sapi_type = php_sapi_name();
		if (substr($sapi_type, 0, 3) == 'cli') {
			return true;
		} else {
			echo "Permission denied.";
			die();
		}
	}
	

	/**
	 * Run migration scripts to the latest version
	 */
	public function latest()
	{
		$this->load->library('migration');

		echo "Migrating...";

		if ($this->migration->latest() === false) {
			show_error($this->migration->error_string());
		}

		echo "Done." . PHP_EOL;
	}
	
	/**
	 * Run migration scripts to the version
	 */
	public function version($version)
	{
		$this->load->library('migration');

		echo "Migrating...";

		if ($this->migration->version($version) === false) {
			show_error($this->migration->error_string());
		}

		echo "Done." . PHP_EOL;
	}
}
