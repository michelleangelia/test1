<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	function __construct()
	{
		parent::__construct();

		$this->load->model('User_Model');
		$this->load->model('Email_Notification_Model');
	}

	function index()
	{
		$this->load->view('layouts/auth', array(
			'body'  => $this->load->view('auth/register', null, true)
		));
	}

	function generate_token(int $length = 16)
	{
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	function save()
	{
		$post = $this->input->post(null, true);

		$password = $post['password'];
		$password_confirmation = $post['password_confirmation'];
		$username = $post['username'];

		if ($password != $password_confirmation) {
			$this->output->http_response(null, 403, 'Password not match.');
		}

		$check = $this->User_Model->where('username', $username)->get();
		if ($check) {
			$this->output->http_response(null, 403, 'Username already exists.');
		}

		$data = array(
			'first_name' => $post['first_name'],
			'last_name' => $post['last_name'],
			'username' => $post['username'],
			'password' => password_hash($post['password'], PASSWORD_DEFAULT),
			'email' => $post['email'],
			'phone' => $post['phone']
		);

		$user_id = $this->User_Model->insert($data);

		$token = $this->generate_token();
		$email = array(
			'user_id' => $user_id,
			'token' => $token,
			'expired' => strtotime("+1 day")
		);

		$this->Email_Notification_Model->insert($email);

		$this->load->library('email');
		$config = Array(
		  'protocol' => 'smtp',
		  'smtp_host' => 'smtp.mailtrap.io',
		  'smtp_port' => 2525,
		  'smtp_user' => '7b41c4b155f153',
		  'smtp_pass' => '1e9631e34f2c20',
		  'crlf' => "\r\n",
		  'newline' => "\r\n"
		);

		$this->email->initialize($config);
		$this->email->from('email@example.com');
		$this->email->to($post['email']);
		$this->email->subject('Confirmation');

		$this->email->set_mailtype("html");
		$this->email->message("<a href='".base_url('register/confirm/'.$token)."'> Please click this link to confirm.</a>");
		$this->email->send();

		$this->output->http_response(null, 200, 'Register success. Please check your email.');
	}

	function confirm($token)
	{
		$check = $this->db->where('token', $token)->where('expired >',time())->get('email_notifications')->row();
		if (isset($check->confirmed)) {
			$data['result'] = "Already actived.";
		} else {
			if ($check) {
				$this->User_Model->update(array('confirmed' => strtotime(date('Y-m-d H:i:s'))), $check->user_id);
				$data['result'] = "The activation successed.";
			} else {
				$data['result'] = "Token expired or user not found.";
			}
		}

		$this->load->view('layouts/auth', array(
			'body'  => $this->load->view('auth/confirm', $data, true)
		));
		
	}

}