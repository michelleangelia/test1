<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_users extends CI_Migration {

    /**
     * Create table properties
     */
    public function up()
    {
        $this->dbforge->add_column('users', array(
            'confirmed' => array(
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'default' => NULL,
            ),
        ));
    }

    /**
     * Drop table properties
     */
    public function down()
    {
        $this->dbforge->drop_column('deleted', 'users');
    }
}