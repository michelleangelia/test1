<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_properties extends CI_Migration {

    /**
     * Create table properties
     */
    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => '200',
            ),
            'description' => array(
                'type' => 'TEXT',
                'default' => ''
            ),
            'slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '200'
            ),
            'price' => array(
                'type' => 'INT',
                'constraint' => '10',
                'default' => 0
            ),
            'price_text' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'default' => ''
            ),
            'created' => array(
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
            ),
            'updated' => array(
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
            ),
            'deleted' => array(
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'default' => NULL,
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (user_id) REFERENCES users(id)');
        $this->dbforge->create_table('properties');
    }

    /**
     * Drop table properties
     */
    public function down()
    {
        $this->dbforge->drop_table('properties');
    }
}