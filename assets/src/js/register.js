$.fn.serializeFormJSON = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


$(document).ready(function() {
	var $form = $('#form-register');

	$form.on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            url: baseUrl + "register/save",
            method: "post",
            dataType: "json",
            data: $form.serializeFormJSON(),
            success: function(response) {
                $('#message').html(response.status.message);
                $('#first-name').val('');
                $('#last-name').val('');
                $('#username').val('');
                $('#email').val('');
                $('#phone').val('');
                $('#password').val('');
                $('#password-confirm').val('');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                var $error = JSON.parse(jqXHR.responseText).status.message;
                $('#message').html ($error);
            }
        });
    });
});